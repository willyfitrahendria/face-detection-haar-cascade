# Face detection using OpenCV with Haar Cascade classifier

Python 3.7.3

OpenCV 4.1.1-dev

Result
---
![](./output/single_result.jpg)
![](./output/group_result.jpg)