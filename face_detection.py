import numpy as np
import cv2
import matplotlib.pyplot as plt

def convertToRGB(image):
	return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
	
def detectFaces(cascade, image, scaleFactor = 1.1):
	# create copy of image
	image_copy = image.copy()
	
	# convert to gray
	image_gray = cv2.cvtColor(image_copy, cv2.COLOR_BGR2GRAY)
	
	# detect face
	faces_rects = cascade.detectMultiScale(image_gray, scaleFactor = 1.2, minNeighbors = 6)
	
	# draw rectangle
	for (x,y,w,h) in faces_rects:
		cv2.rectangle(image_copy, (x, y), (x+w, y+h), (0, 255, 0), 3)

	return image_copy

# load image
image_single = cv2.imread('single.jpg')
image_group = cv2.imread('group.jpg')

# load frontal face classifier
haar_cascade_face = cv2.CascadeClassifier('data/haarcascades/haarcascade_frontalface_default.xml')

# detect faces
image_single_faces = detectFaces(haar_cascade_face, image_single)

#show the image
plt.imshow(convertToRGB(image_single_faces))
plt.show()

# detect faces
image_group_faces = detectFaces(haar_cascade_face, image_group)

#show the image
plt.imshow(convertToRGB(image_group_faces))
plt.show()

